import React from 'react';
import {useState} from 'react';

//Bootstrap
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function WorkerInfo(props) {
    const tempWorkers = [...props.workers];

    const removeWorker = (index) => {
        console.log(tempWorkers)
        tempWorkers.splice(index, 1);
        props.setWorkers(tempWorkers);
    }

    const editWorker = (index) => {
          tempWorkers[index] = {
            firstName: props.firstName,
            lastName: props.lastName,
            position: props.position,
            team: props.team
        };
        props.setWorkers(tempWorkers);
    };

    const workerList = props.workers.map((item, index) => {
        return(
                    <Col sm>
                        <Card className="worker-info-card">
                            <Card.Body>
                                <Card.Title>{item.lastName}</Card.Title>
                                <Card.Subtitle className="mb-2 font-weight-bold">{item.firstName}</Card.Subtitle>
                                <Card.Subtitle className="mb-2 text-muted">Pozícia: {item.position}</Card.Subtitle>
                                <Card.Subtitle className="mb-2 text-muted">Tím: {item.team}</Card.Subtitle>

                                <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button type="button" className="btn btn-outline-secondary" onClick={() => editWorker(index)}>Upraviť</button>
                                    <button type="button" className="btn btn-outline-danger" onClick={() => removeWorker(index)}>Zmazať</button>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
        )
    })

    return (
        <div>
            <Container>
                    <Row xs={1} sm={2} md={4}>
                        {workerList}
                    </Row>
            </Container>
        </div>
    )
}
