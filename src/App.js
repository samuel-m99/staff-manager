import {useState} from 'react';

//Bootstrap
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

//Styles
import "./App.css";
import "./Components/input-field/InputField.css"
import "./Components/worker-info/WorkerInfo.css"
import "./Components/worker-list/WorkerList.css"

//Functions
import InputField from "./Components/input-field/InputField.js"
import WorkerInfo from "./Components/worker-info/WorkerInfo.js"
import AddWorker from "./Components/add-worker/AddWorker.js"
import WorkerList from "./Components/worker-list/WorkerList.js"

function App() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [position, setPosition] = useState("");
  const [team, setTeam] = useState("");
  const [workers, setWorkers] = useState([]);

  return (
          <div>
            <div className="input-card-position">
              <Card className="card-bg">
                <Card.Body className="card-style">
                  <InputField
                    label="Meno"
                    value={firstName}
                    setValue={setFirstName}
                  />
                  <InputField
                    label="Priezvisko"
                    value={lastName}
                    setValue={setLastName}
                  />
                  <InputField
                    label="Pozícia"
                    value={position}
                    setValue={setPosition}
                  />
                  <InputField
                    label="Tím"
                    value={team}
                    setValue={setTeam}
                  />

                    <div>
                      <AddWorker
                        firstName={firstName}
                        lastName={lastName}
                        position={position}
                        team={team}
                        workers={workers}
                        setWorkers={setWorkers}

                        //Props for resetting state after submitting worker
                        setFirstName={setFirstName}
                        setLastName={setLastName}
                        setPosition={setPosition}
                        setTeam={setTeam}
                      />
                    </div>
                  </Card.Body>
                </Card>
            </div>

            <div className="worker-list">
              <WorkerList
                firstName={firstName}
                lastName={lastName}
                position={position}
                team={team}
                workers={workers}
                setWorkers={setWorkers}
              />
            </div>
          </div>
  );
};

export default App;
